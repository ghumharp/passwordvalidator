package password;
/**
 * 
 * @author Harpreet Ghuman 991543936
 * 
 * this class will validate the length of the password
 * it is assumed that spaces should not be considered valid characters for the purpose of calculating length
 * when checking number of digits added counter to keep count of digits in the password
 *
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean hasValidCaseChars( String password ) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	public static boolean isValidLength(String password) {
		/*
		boolean valid;
		if(password.length()>=8)
			valid = true;
		else
			valid = false;
		
		return valid;
		*/
		return(password !=null && !password.contains(" ") && password.length()>=MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int count =0;
		char c;
		for(int i =0; i<password.length(); i++) {
			c=password.charAt(i);
			if(Character.isDigit(c)){
				count++;
				if(count>=2) {
					return true;
				}
			}
		}
		
		return false;
	}

}
