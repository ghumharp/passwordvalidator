package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Harpreet Ghuman 991543936
 * 
 * this class will validate the length of the password
 *
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		//fail("Invalid case chars");
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("fdfdRfff"));
	}
	@Test
	public void testHasValidCaseCharsBoundryIn() {
		//fail("Invalid case chars");
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		//fail("Invalid case chars");
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		//fail("Invalid case chars");
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		//fail("Invalid case chars");
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	@Test
	public void testHasValidCaseCharsExceptionBoundryOutUpper() {
		//fail("Invalid case chars");
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	@Test
	public void testHasValidCaseCharsExceptionBoundryOutLower() {
		//fail("Invalid case chars");
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	/*
	@Test
	public void testHasValidDigitCountRegular() {
		//fail();
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("He11oW0rld");
		
	}
	
	@Test
	public void testHasValidDigitCountException() {
		//fail();
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("HelloWorld");
		assertFalse("Invalid Password Digit Count", hasValidDigitCount);	
	}
	
	@Test
	public void testHasValidDigitCountBoundryIn() {
		fail();
		
		//boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("He11oWorld");
		//assertTrue("Invalid Password Digit Count", hasValidDigitCount);
		
	}
	
	@Test
	public void testHasValidDigitCountBoundryOut() {
		//fail();
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("He1loWorld");
		assertFalse("Invalid Password Digit Count", hasValidDigitCount);	
	}
	
	
	@Test
	public void testIsValidLengthRegular() {
		//fail("Invalid password length");
		boolean isValidLength = PasswordValidator.isValidLength("123456789789");
				assertTrue("Invalid Password Length", isValidLength);		
	}	
	
	@Test
	public void testIsValidLengthException() {
		//fail();
		boolean isValidLength = PasswordValidator.isValidLength("12345");
				assertFalse("Invalid Password Length", isValidLength);
	}	
	
	@Test
	public void testIsValidLengthBoundryOut() {
		//fail();
		boolean isValidLength = PasswordValidator.isValidLength("1234567");
				assertFalse("Invalid Password Length", isValidLength);
	}
	
	@Test
	public void testIsValidLengthBoundryIn() {
		//fail("Invalid password length");
		boolean isValidLength = PasswordValidator.isValidLength("12345678");
				assertTrue("Invalid Password Length", isValidLength);
	}
	@Test
	public void testIsValidLengthExceptionSpace() {
		//fail();
		boolean isValidLength = PasswordValidator.isValidLength("        ");
				assertFalse("Invalid Password Length", isValidLength);
	}
	*/
}
	
	
